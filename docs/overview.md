# Overview

## Description

This gear classifies files based on 1) associated metadata and 2) a classification
 profile supplied containing declarative rules that act on the metadata. The profile is
 highly customizable and can act based on any metadata field provided by the input file.

## Input

This gear takes one required input file, and optionally a custom profile file.
Currently the gear supports classification of the following file types:

- DICOM and DICOM Zip Archive
  - via the file.info.header.dicom namespace which is populated using the
  [file-metadata-importer](
    https://flywheel-io.gitlab.io/scientific-solutions/gears/file-metadata-importer/)
    gear
- NIfTI
  - via a json sidecar which is found in the same container as the input.

By default, we provide a set of classification profiles that you can use to classify
medical DICOM images with the following modalities:

- Computed Tomography (CT)
- Magnetic Resonance (MR)
- Positron Emission Tomography (PT)
- Mammography (MG)

However, you can also create custom classification profiles to classify other types of
files.

## Output

The gear does not output any files. The gear will modify the original metadata to add a
classification field. For example the classification appended might appear similar to:

!!! example
    ```json
    {
        "file": {
            "classification": {
                "Intent": "Structural",
                "Measurement": "T1",
            }
        }
    }
    ```

## Classification
<!-- markdownlint-disable MD032 -->
- Maintainer: <img src="https://img.shields.io/badge/Flywheel-1B68FA" alt="Flywheel">  
- Therapeutic Area: <img src="https://img.shields.io/badge/Any-1B68FA" alt="Any">
- Modality:
<img src="https://img.shields.io/badge/CT-1B68FA" alt="CT">
<img src="https://img.shields.io/badge/MR-1B68FA" alt="MR">
<img src="https://img.shields.io/badge/PT-1B68FA" alt="PT">
<img src="https://img.shields.io/badge/MG-1B68FA" alt="MG">
- Suite: <img src="https://img.shields.io/badge/Utility-1B68FA" alt="Utility">  
- File Type:
<img src="https://img.shields.io/badge/dicom-1B68FA" alt="dicom">
<img src="https://img.shields.io/badge/nifti-1B68FA" alt="nfiti">
- GPU: <img src="https://img.shields.io/badge/No-1B68FA" alt="No">
<!-- markdownlint-enable MD032 -->
