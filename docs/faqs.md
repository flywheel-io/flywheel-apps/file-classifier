# Frequently Asked Questions

??? question "How do I create a custom classification profile?"
    <!-- markdownlint-disable MD013 -->
    You can create a custom classification profile by following the instructions in the
    [profile](https://flywheel-io.gitlab.io/scientific-solutions/lib/fw-classification/fw-classification/profile/)
    section of the fw-classification library documentation.
    <!-- markdownlint-enable MD013 -->

??? question "I ran the classifier gear but my file is not classified. What's wrong?"
    The first thing you should check is the logs of the gear job to see if there were any
    obvious errors that would cause the gear to fail. If there are no errors, the logs
    also produce messages which indicate which portions of the classification profile
    evaluated to true. If the issue is still unclear, you should check to see if the
    file has the necessary metadata for the profiles to work. If the file does not have
    the necessary metadata, the classifier gear will not be able to classify the file.
    Often times, this is because either the `file-metadata-importer` has not
    been run on the file, or the file has been stripped of metadata in some way (e.g.
    anonymized/deidentified).

??? question "What resources are needed to run the gear?"
    This gear can be run on any instance type and does not require any specialized
    hardware.

??? question "How long does it take to run the gear?"
    This gear usually takes a few seconds (<10s) to run.
