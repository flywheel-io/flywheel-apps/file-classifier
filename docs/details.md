<!-- markdownlint-disable MD013 -->
# Gear Details

This section provides detailed information about the gear, including its inputs,
outputs, and configuration options.

## License

[MIT
License](https://gitlab.com/flywheel-io/scientific-solutions/gears/file-classifier/-/blob/main/LICENSE)

## Classification

* **Species:** Any
* **Organ:** Any
* **Therapeutic Area:** Any
* **Modality:** CT, MR, PT, MG
* **File Type:** DICOM, NIfTI
* **Function:** Utility
* **Suite:** Utility
* **Category:** Utility
* **Gear Permission:** Read-Write
* **Container Level:**
  * [X] Project
  * [X] Subject
  * [X] Session
  * [X] Acquisition
  * [X] Analysis

## Inputs

### Files

#### input-file

* **Type**: DICOM or NIfTI
* **Optional**: `false`
* **Description**: Input File

#### profile

* **Type**: YAML
* **Optional**: `true`
* **Description**: Optional classification profile

### Configuration

#### debug

* **Type**: `bool`
* **default**: `false`
* **description**: When enabled, adds debug statements in log.

#### remove_existing

* **Type**: `bool`
* **default**: `false`
* **description**: When enabled, clear existing classification.

#### tag

* **Type**: `string`
* **default**: "file-classifier"
* **description**: The tag to be added to input file upon run completion.

#### validate

* **Type**: `bool`
* **default**: `false`
* **description**: When enabled, validate the classification schema.

## Outputs

By default, file classification metadata is stored under `file.info.classification`
in accordance with the schema defined on the user's Flywheel instance. Custom metadata
can be defined in the classification profile and stored under
`file.info.classification.custom`.

## Custom Classification Profiles

Detailed documentation for creating classification profiles can be found in the
[profile](
  https://flywheel-io.gitlab.io/scientific-solutions/lib/fw-classification/fw-classification/profile/) section of the fw-classification library documentation.

Custom classification profiles can be included several ways:

### 1. Custom Profile File (Recommended)

You can create a custom classification profile and include it as an input file when
configuring the gear. This completely overrides the default classification profiles, so
you may want to include the default profiles, or sections of the profiles in your
custom profile if you wish to retain some of the default behavior. You can find the
default profiles in the
[fw-classification-profiles](
  https://gitlab.com/flywheel-io/scientific-solutions/lib/fw-classification-profiles)
  library.

Here is an example of a custom classification profile. This profile will set the
`custom` field to `Deleted` if the `ProtocolName` field is `Deleted` in the DICOM
header.

??? example "Set custom deleted classification if ProtocolName was deleted."

    ```yaml
    name: Custom classifier
    includes:
      # Include default MR
      - https://gitlab.com/flywheel-io/scientific-solutions/lib/fw-classification-profiles$profiles/MR.yaml


    profile:
      - name: set_custom_deleted
        description: |
          Set custom deleted classification if ProtocolName was deleted
        rules:
          - match_type: 'all'
            match:
              - key: file.type
                is: dicom
              - key: file.info.header.dicom.ProtocolName
                is: 'Deleted'
            action:
              - key: file.classification.Custom
                add: 'Deleted'

    ```

### 2. Contextual classifications via in project custom information

You can also include a custom classification blocks by adding them to the **project
custom information**. This is useful when you want to extend the current classification
with a small set of rules for a given project. In this case, the rules are appended
after the profiles have been selected (either default or by the
custom profile method above). This can be done via the UI or the SDK.

!!! note
    This method will append rules to the classification profile after profile selection,
    which means that they will have the highest priority.

Here is an example of adding a custom block using the SDK for this method.

??? example "Adding ProtocolName block via SDK"

    ``` python
    import flywheel
    fw = flywheel.Client()
    proj = fw.get_project(<proj_id>) # or use lookup()
    existing_info = proj.info

    # Initialize context classifications if they don't exist
    existing_info.setdefault('classifications', [])
    existing_info['classifications'].append(
        {
            'match': [
                {
                    'key': 'file.type',
                    'is': 'dicom',
                },
                {
                    'key': 'file.info.header.dicom.ProtocolName',
                    'is': 'deleted',
                }
            ],
            'action': [
                {'key': 'file.classification.Custom', 'add': 'Deleted'},
            ]
        }
    )
    proj.replace_info(existing_info)
    ```

And here is an example of adding a custom block using the user interface. You first need to to navigate to the project "Information" tab for your project, then you can and add the following block to the "custom information" section.

??? example "Adding ProtocolName block via the UI"
    ![Custom Block](assets/images/custom-classifications-ui.png "Custom Block")
