# File Classifier Gear Documentation

Welcome to the documentation for the File Classifier gear. This gear is designed
to help you classify files based on their metadata. The gear is meant to be a general
tool to classify any files based on associated metadata, and it is built on top of the
[fw-classification](
    https://flywheel-io.gitlab.io/scientific-solutions/lib/fw-classification) toolkit.

In this documentation, you will find detailed information on how to use the File
Classifier gear, including usage examples and advanced features.

Finally if you have any questions about how to use the gear, please feel free to reach
out to us by reaching out to <support@flywheel.io>

Let's get started!
