# Usages and Workflows

The `file-classifier` gear is designed to help you automatically classify your files with
useful metadata so that they can be easily identified and searched in Flywheel. The
gear can be used in a variety of ways, depending on your specific needs and
requirements. This section will cover the different usages and workflows of the gear.

## Synergy with other gears

The `file-classifier` gear is part of suite of gears that are designed to work
synergistically to help you curate and manage your files. In this case the
file-classifier gear is designed to run after the `file-metadata-importer` gear to
classify files and augment the metadata extracted by the `file-metadata-importer` gear.

## Gear Rules

The `file-classifier` gear is also a good candidate to be run as gear rule
automatically on file upload (triggered after the `file-metadata-importer` gear). This
also allows for more advanced workflows to run via gear rules triggered on specific
classifications. For example if you have a rule that classifies a file as a T1-weighted
MRI image, you can then trigger a gear rule to automatically run a gear that processes
T1-weighted MRI images.
