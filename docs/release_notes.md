# Release notes

## 0.6.10

__Enhancement__:

- Update classification profile to 0.4.9

## 0.6.9

__Enhancement__:

- Update classification profile to 0.4.8

## 0.6.8

__Bug Fix__:

- Fix missing git tool in dockerfile.

__Enhancement__:

- Update classification profile to 0.4.7

## 0.6.7

__Bug Fix__:

- Fix environment variables in manifest.json

## 0.6.6 (yanked)

__Bug Fix__:

- Fix AC -> Attenuation Corrected in PT profile (v 0.4.5)
- Fix Import fallback profile to classify MR files lacking 'Measurement' field (v 0.4.4)

__Maintenence__:

- Populate-gear-manifest-with-categorization

## 0.6.5

__Bug Fix__:

- Fix pydantic issues by updating profiles to 0.4.3

__Maintenence__:

- Populate-gear-manifest-with-categorization

## 0.6.4

__Bug Fix__:

- Revert file-type changes. Replaces 0.6.3

## 0.6.3

__Enhancement__:

- Update classification-profiles to 0.4.1
  - Initial CT profiles
  
- Update to fw-classification v0.4.4
  - JSON sidecars apply a timing conversion from BIDS->DICOM units.

__Bug Fix__:

- Update classification-profiles to 0.4.1
  - Fix intent classifier bug

## 0.6.2

__Enhancement__:

- Update classification-profiles to 0.4.0. This adds physics basgit ed MR
profiles and MG profile.
- Add configuration option to clear existing classification on a file.

## 0.6.1

__Maintenance__:

- Update classification-profiles to 0.3.1 to fix contrast key issue.
- Fix pipeline
- Update url and source link

## 0.6.0

__Enhancement__:

- Adding config option to validate the UI schema
- Update classification profile to 0.3.0 to enable PT classification

## 0.5.1

__Enhancement__:

- Allow referencing built-in profiles w/o git

## 0.5.0

__Maintenance__:

- Update fw-classification
- Update CI

__Enhancement__:

- Update to newer GTK metadata methods
- Reorganize output QC info.

## 0.4.1-0.4.6

__Maintenance__:

- Update classification profiles to 0.2.2
- Update CI

## 0.4.0

__Maintenance__:

- Remove classifications as context input
- Check for project classifications via API
- Update fw-classification to include classifications on bval/bvec files.

## 0.3.0

__Maintenance__:

- Make context classifications an optional input.

## 0.2.0

__Enhancement__:

- Allow passing in a git profile via the config `profile` option.
- Improve logging around profile selection.

__Documentation__:

- Update docs on priority of profiles in profile selection.

## 0.1.0

Initial release
