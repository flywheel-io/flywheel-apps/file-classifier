FROM alpine/git:v2.36.1 AS profiles

ENV PROFILE_VERSION=0.4.9

RUN git clone --depth 1 \
    --branch $PROFILE_VERSION \
    https://gitlab.com/flywheel-io/public/fw-classification/fw-classification-profiles.git \
    /root/profiles/

FROM flywheel/python:3.10

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

# Install git (required for downloading profiles)
RUN apk add --no-cache git

# Installing main dependencies
COPY requirements.txt $FLYWHEEL/
RUN pip install --no-cache-dir -r $FLYWHEEL/requirements.txt

# Installing the current project (most likely to change, above layer can be cached)
COPY ./ $FLYWHEEL/
RUN pip install --no-cache-dir .

# Copying profiles
COPY --from=profiles /root/profiles/classification_profiles ./fw_gear_file_classifier/classification_profiles

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["python","/flywheel/v0/run.py"]
