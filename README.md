# File Classifier

`file-classifier` is a Flywheel Gear that imports metadata header of
file and indexes them in Flywheel.

## Documentation

The documentation of the gear can be found
[here](https://flywheel-io.gitlab.io/scientific-solutions/gears/file-classifier/index.html).

## Contributing

Please refer to the [CONTRIBUTING.md](CONTRIBUTING.md) file for information on how to
contribute to the gear.
